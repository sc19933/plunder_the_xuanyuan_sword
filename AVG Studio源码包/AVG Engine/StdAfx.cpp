// stdafx.cpp : source file that includes just the standard includes
//	AVG.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file
void TransBlt( HDC hdc,int ox,int oy,int cx,int cy,HDC hMemDC,int sx,int sy,COLORREF colorkey )
{
	COLORREF crOldBack = ::SetBkColor( hdc, RGB(255,255,255) );
	COLORREF crOldText = ::SetTextColor( hdc, RGB(0,0,0) );
	COLORREF crOldBackImg = ::SetBkColor( hMemDC, colorkey );
	COLORREF crOldTextImg = ::SetTextColor( hMemDC, RGB(0,0,0) );


	HDC hMaskDC  = ::CreateCompatibleDC( hdc );

	HBITMAP hBmpMask = ::CreateBitmap( cx, cy, 1, 1, NULL );
	HBITMAP hOldBmp  = (HBITMAP)::SelectObject( hMaskDC, hBmpMask );

	::SetBkColor( hMaskDC, RGB(255,255,255) );
	::SetTextColor( hMaskDC, RGB(0,0,0) );


	BitBlt( hMaskDC, 0, 0, cx, cy, hMemDC, sx, sy, SRCCOPY );
	BitBlt( hdc, ox, oy, cx, cy, hMemDC, sx, sy, SRCINVERT );
	BitBlt( hdc, ox, oy, cx, cy, hMaskDC, 0, 0, SRCAND );
	BitBlt( hdc, ox, oy, cx, cy, hMemDC, sx, sy, SRCINVERT );

	hBmpMask = (HBITMAP)::SelectObject( hMaskDC, hOldBmp );
	::DeleteObject( hBmpMask );
	::DeleteDC( hMaskDC );

	::SetBkColor( hdc, crOldBack );
	::SetTextColor( hdc, crOldText );
	::SetBkColor( hMemDC, crOldBackImg );
	::SetTextColor( hMemDC, crOldTextImg );
}