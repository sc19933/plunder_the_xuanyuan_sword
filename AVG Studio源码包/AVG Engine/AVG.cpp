// AVG.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include "DS.h"
#include "FS.h"
#include "DB.h"

#define MAX_LOADSTRING 100


//
//*******************************************************
//成员变量定义
CMidi	m_Midi;		//用于背景音乐和声音播放的库
CGameData	m_DB;	//用于存放和操作“系统数据堆”的类实例
CDS		m_DS;		//对话场的类实例
CFS		m_FS;		//战斗场的类实例
//*******************************************************

HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_AVG, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_AVG);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 

	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
};
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_AVG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	//下面的语句修改了，屏蔽菜单。
	wcex.lpszMenuName	= NULL;//(LPCSTR)IDC_AVG;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
};
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(
					szWindowClass,//窗口类名称
					szTitle, //窗口标题
					WS_POPUP | WS_DLGFRAME, //窗口风格
					( (GetSystemMetrics(SM_CXSCREEN) -646) /2 ),
					( (GetSystemMetrics(SM_CYSCREEN) -486) /2 ),//使窗口居中屏幕显示
					646, //窗口的宽度
					486, //窗口的高度
					NULL, //父窗口句柄
					NULL, //菜单句柄
					hInstance, //应用程序实例句柄
					NULL ); //窗口创建数据指针
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);
	//****************************
	//此处添加初始化程序段
	m_DB.init();
	m_DS.Init( hWnd,&m_Midi,&m_DB );
	m_FS.Init( hWnd,&m_Midi,&m_DB );
	SetTimer( hWnd, 1000, 60, NULL );	//启动动画绘制定时器
	//初始化程序段结束
	//****************************
   return TRUE;
};
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case MM_MCINOTIFY:
			m_Midi.Replay(wParam);
			break;
//************************************************************************
		case WM_LBUTTONDOWN://鼠标左键消息
			if( m_DB.GetCurSystem() == 1 )
			{	//对话场
				m_DS.OnLButtonDown(LOWORD(lParam),HIWORD(lParam));
			}
			else if( m_DB.GetCurSystem() == 2 )
			{	//战斗场
				m_FS.OnLButtonDown(LOWORD(lParam),HIWORD(lParam));
			}
			break;
		case WM_RBUTTONDOWN://鼠标右键消息处理系统对话框
			KillTimer(hWnd,1000);				//暂停动画绘制定时器
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			SetTimer( hWnd, 1000, 60, NULL );	//启动动画绘制定时器
			break;
		case WM_TIMER://定时器消息
			if( m_DB.GetCurSystem() != 0 )
			{
				if( m_DB.GetCurSystem() == 1 )
				{	//对话场
					m_DS.OnTimerPrc( hWnd );
				}
				else if( m_DB.GetCurSystem() == 2 )
				{	//战斗场
					m_FS.OnTimerPrc( hWnd );
				}
			}
			else	//系统状态
			{
				//1、获取游戏进度ID并配置两个文件名
				int ID = m_DB.Get( 1 );
				char fname1[255];
				char fname2[255];
				itoa( ID, fname1, 10 );
				strcat( fname1, ".dsn" );	//给对话场用
				itoa( ID, fname2, 10 );
				strcat( fname2, ".fsn" );	//给战斗场用
				//2、看“对话场”是否能找到匹配的文件
				if( m_DS.Open(fname1) )
				{
					m_DB.SetCurSystem( 1 );
				}
				//3、看“战斗场”是否能找到匹配的文件
				else if( m_FS.Open(hWnd,fname2) )
				{
					m_DB.SetCurSystem( 2 );
				}
				//4、都找不到则出错
				else
				{
					KillTimer(hWnd,1000);
					MessageBox(hWnd,"找不到匹配的剧情，请检查上一个剧情段落的出口设置。",NULL,MB_ICONERROR|MB_OK);
					::PostQuitMessage( WM_QUIT );
				}
			}
			break;
//************************************************************************
		case WM_DESTROY:
			//退出时关闭Midi
			m_Midi.Stop();
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
};
//
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;
		case WM_COMMAND:
			if( LOWORD(wParam) == IDSAVE )
			{	//存盘
				m_DB.Save( "game.sav" );
			}
			else if( LOWORD(wParam) == IDOPEN )
			{	//读盘
				m_DB.Open( "game.sav" );
				m_DB.SetCurSystem( 0 ); //强迫返回系统状态，以便从新解读系统数据堆
			}
			else if( LOWORD(wParam) == IDEXIT )
			{
				m_Midi.Stop();
				PostQuitMessage(WM_QUIT);
			}
			else if( LOWORD(wParam) == IDOK )		//返回
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
};