void CMainFrame::Para3Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para3.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic322.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//西蒙镇长
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic323.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡洛德镇长
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic316.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（2）
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic309.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//医生哈德尔
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic306.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//露丝
	hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic303.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//教徒
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	hbmp_Prop0=(HBITMAP)LoadImage(NULL,"pic\\pic202.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//信
	hbmp_Prop1=(HBITMAP)LoadImage(NULL,"pic\\pic206.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//瑞士军刀
	::DeleteObject(hbmp_Prop2);//释放
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic111.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//露丝
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic103.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic110.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子和教徒
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic017.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//火山
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
	if(st_sub1==0){
		m_info_st=9;//对话态
		m_oldinfo_st=9;
	}
/////////////////////////////////////////////////////////////////////////////
}//OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图的语句
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para3Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0)
	{
		Dialog(2,0,0,0,1);//去望山镇送信
		st_dialog=1;
	}
	else if(st_dialog==1)
	{
		PlayMidi("midi\\p1003.mid");
		OpenMap("map\\maps103.bmp","map\\map103c.map","map\\map103d.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=287;///////////////
		current.x=97;
		current.y=125;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_RIGHT;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=0;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==30)
	{//诊所
		Dialog(4,36,0,0,5);
		st_dialog=20;
	}
	else if(st_dialog==31)
	{//山洞捡到瑞士军刀
		Dialog(4,0,31,0,8);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{//山洞捡到瑞士军刀
		PlayWave("snd//prop.wav");
		m_info_prop2=7;
		CDC *pdc=GetDC();
		RenewInfo(pdc);//显示瑞士军刀
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,31,2,9);
		GameSleep(500);
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108d.npc");//更换地图
		st_sub1=1;
		st_dialog=20;
	}
}//以拿到瑞士军刀为界
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){
	if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	if(st_dialog==30)
	{//遇见卡洛德
		Dialog(4,16,0,0,14);
		m_info_prop1=0;//清除书信
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,0,31,0,15);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,16,0,0,16);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		st_sub1=2;//进入下一个进度中
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108c.npc");//不许下山的地图
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=1;//准备询问村民
	}
}//以送信给卡洛德为界
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==2){
	if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==30)
	{
		Dialog(4,16,0,0,27);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,16,0,0,28);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{//文字蓝色，卡诺：“难道真的有剑圣？”
		Dialog(3,0,31,0,91);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{//得到书信
		PlayWave("snd//prop.wav");
		m_info_prop1=3;
		CDC *pdc=GetDC();
		RenewInfo(pdc);//显示书信
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,1,29);
		GameSleep(500);
		st_dialog=34;
	}
	else if(st_dialog==34)
	{
		//Dialog(5,0,0,0,85);
		//st_dialog=35;
		Dialog(2,0,0,0,92);
		st_sub1=3;
		st_dialog=0;
	}
}//以村长交付回信为界
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==3){
	if(st_dialog==0)
	{
		//用于绘制“请稍候……”
		CDC *pdc=GetDC();
		RECT rect;
		rect.left=0;
		rect.top=400;
		rect.right=799;
		rect.bottom=479;
		pdc->SetTextColor(0x0000ffff);//黄色
		pdc->SetBkMode(TRANSPARENT);//设置透明背景
		pdc->DrawText("请稍侯……",10,&rect,DT_CENTER);
		ReleaseDC(pdc);
		pdc = NULL;
		//
		PlayMidi("midi\\p1008.mid");
		Dialog(2,0,0,0,92);
		st_dialog=1;
	}
	else if(st_dialog==1)
	{//初始化地图
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108b.npc");//遇荻娜的地图
		map[49][142]=755;
		map[49][143]=795;//修改地图
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		CDC *pdc=GetDC();
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==30)
	{
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		CDC *pdc=GetDC();
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);//用于擦去叹
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,23,0,0,31);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,0,31,0,32);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,22,0,0,33);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		map[49][142]=188;
		map[49][143]=188;//修改地图
		current.x=49;
		current.y=143;
		azimuth= AZIMUTH_DOWN;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		DrawActor(1);
		CDC *pdc=GetDC();
  		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);//送屏幕显示
		ReleaseDC(pdc);
		pdc = NULL;
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108c.npc");//遇荻娜后不许上山的地图
		st_sub1=4;
		st_dialog=20;
		Dialog(2,0,0,0,23);
	}
}//以遇见荻娜为界
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==4){
	if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==30)
	{
		m_info_st=9;//对话态
		m_oldinfo_st=9;
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		CDC *pdc=GetDC();
		blt(416,208,32,48,160,192,64,192,pdc);//绘制歪倒的荻娜
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,23,0,0,35);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,0,31,0,36);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,36,0,0,37);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		vclose();
		m_info_prop1=0;//擦去书信
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,88);
		st_dialog=34;
	}
	else if(st_dialog==34)
	{
		OpenMap("map\\maps103.bmp","map\\map103d.map","map\\map103e.npc");//更换荻娜卧床的地图
		current.x=45;
		current.y=90;
		azimuth= AZIMUTH_LEFT;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		DrawActor(1);
		vopen();
		Dialog(4,36,0,0,39);
		st_dialog=35;
	}
	else if(st_dialog==35)
	{
		Dialog(4,22,0,0,40);
		st_dialog=36;
	}
	else if(st_dialog==36)
	{
		Dialog(4,36,0,0,41);
		st_dialog=37;
	}
	else if(st_dialog==37)
	{
		Dialog(4,36,0,0,42);
		st_dialog=38;
	}
	else if(st_dialog==38)
	{//白莲教出场
		vclose();
		PlayMidi("midi\\p1011.mid");
		current.x=15;
		current.y=136;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		vopen();
		//船开出
		MemDC.SelectObject(hbmp_4);//白莲教团伙
		CDC *pdc=GetDC();
		for(i=0;i<16;i++){//移动
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//背景
			BuffDC.BitBlt(300,256,64,96,&MemDC,192,320,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,0,320,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,256,320,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,64,320,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,192,320,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,0,320,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,256,320,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,64,320,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			current.x+=1;
		}//上面部分是在水中，下面部分是上岸
		current.y-=1;
		for(i=0;i<6;i++){//移动
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//背景
			BuffDC.BitBlt(300,256,64,96,&MemDC,192,224,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,0,224,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,256,224,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,64,224,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,192,224,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,0,224,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			BuffDC.BitBlt(300,256,64,96,&MemDC,256,224,SRCAND);
			BuffDC.BitBlt(300,256,64,96,&MemDC,64,224,SRCINVERT);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(50);
			current.x+=1;
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,46,0,0,43);
		st_dialog=39;
	}
	else if(st_dialog==39)
	{
		Dialog(4,0,6,0,44);
		st_dialog=40;
	}
	else if(st_dialog==40)
	{
		Dialog(4,46,0,0,45);
		st_dialog=41;
	}
	else if(st_dialog==41)
	{
		Dialog(4,0,8,0,46);
		st_dialog=42;
	}
	else if(st_dialog==42)
	{//餐馆的场景
		//汗……
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(328,224,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(296,256,35,33,0,0,35,0,pdc);//汗
		resetblt();
		blt(328,288,35,33,0,0,35,0,pdc);//汗
		GameSleep(1000);
		resetblt();
		blt(328,224,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(296,256,35,33,0,0,70,0,pdc);//汗
		resetblt();
		blt(328,288,35,33,0,0,70,0,pdc);//汗
		GameSleep(2000);
		vclose();//出汗结束
		//下面的故事在餐馆发生。
		PlayMidi("midi\\p1001.mid");//变换音乐
		current.x=114;
		current.y=170;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		//准备人物。（荻娜、卡诺、露丝、司徒）
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(224,256,32,48,128,144,32,144,&BuffDC);//荻娜
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(320,256,32,48,96,48,0,48,&BuffDC);//卡诺
		MemDC.SelectObject(hbmp_1);//露丝
		resetblt();
		blt(256,208,32,48,96,0,0,0,&BuffDC);//露丝
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(448,64,32,32,64,64,0,64,&BuffDC);//司徒
		vopen();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,21,0,0,47);
		st_dialog=43;
	}
	else if(st_dialog==43)
	{
		Dialog(4,0,32,0,48);
		st_dialog=44;
	}
	else if(st_dialog==44)
	{
		Dialog(4,21,0,0,49);
		st_dialog=45;
	}
	else if(st_dialog==45)
	{
		Dialog(4,0,41,0,50);
		st_dialog=46;
	}
	else if(st_dialog==46)
	{
		Dialog(4,0,31,0,51);
		st_dialog=47;
	}
	else if(st_dialog==47)
	{
		Dialog(4,21,0,0,52);
		st_dialog=48;
	}
	else if(st_dialog==48)
	{
		Dialog(4,0,41,0,53);
		st_dialog=49;
	}
	else if(st_dialog==49)
	{
		Dialog(4,26,0,0,55);
		st_dialog=50;
	}
	else if(st_dialog==50)
	{
		Dialog(4,0,31,0,54);
		st_dialog=51;
	}
	else if(st_dialog==51)
	{
		Dialog(4,0,41,0,56);
		st_dialog=52;
	}
	else if(st_dialog==52)
	{
		Dialog(4,0,32,0,57);
		st_dialog=53;
	}
	else if(st_dialog==53)
	{//露丝去取酒
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//露丝
		resetblt();
		blt(256,144,32,48,96,144,0,144,pdc);//露丝
		for(i=0;i<6;i++)
		{
			blt((256+(i*16)),208,32,48,128,144,32,144,pdc);//露丝
			GameSleep(50);
			blt((272+(i*16)),208,32,48,160,144,64,144,pdc);//露丝
			GameSleep(50);
		}
		for(i=0;i<16;i++)
		{
			blt(352,(208-(i*16)),32,48,128,96,32,96,pdc);//露丝
			GameSleep(50);
			blt(352,(192-(i*16)),32,48,160,96,64,96,pdc);//露丝
			GameSleep(50);
		}
		GameSleep(200);
		for(i=0;i<16;i++)
		{
			blt(352,(-48+(i*16)),32,48,128,0,32,0,pdc);//露丝
			GameSleep(50);
			blt(352,(-32+(i*16)),32,48,160,0,64,0,pdc);//露丝
			GameSleep(50);
		}
		for(i=0;i<6;i++)
		{
			blt((352-(i*16)),208,32,48,128,48,32,48,pdc);//露丝
			GameSleep(50);
			blt((336-(i*16)),208,32,48,160,48,64,48,pdc);//露丝
			GameSleep(50);
		}
		blt(256,208,32,48,96,0,0,0,pdc);//露丝
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,41,0,58);
		st_dialog=54;
	}
	else if(st_dialog==54)
	{
		Dialog(4,0,31,0,59);
		st_dialog=55;
	}
	else if(st_dialog==55)
	{
		Dialog(4,21,0,0,60);
		st_dialog=56;
	}
	else if(st_dialog==56)
	{
		Dialog(4,0,31,0,61);
		st_dialog=57;
	}
	else if(st_dialog==57)
	{
		//显示叹号
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(448,31,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		resetblt();
		blt(448,31,35,33,0,0,140,0,pdc);//叹
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		resetblt();
		blt(448,31,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		resetblt();
		blt(448,31,35,33,0,0,140,0,pdc);//叹
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		//司徒走过来
		MemDC.SelectObject(hbmp_2);//司徒
		blt(448,64,32,32,64,96,0,96,pdc);//司徒
		for(i=0;i<4;i++)
		{
			blt((448-(i*16)),64,32,32,96,96,32,96,pdc);//司徒
			GameSleep(50);
			blt((432-(i*16)),64,32,32,64,96,0,96,pdc);//司徒
			GameSleep(50);
		}
		for(i=0;i<16;i++)
		{
			blt(368,(64+(i*16)),32,32,96,64,32,64,pdc);//司徒
			GameSleep(50);
			blt(368,(80+(i*16)),32,32,64,64,0,64,pdc);//司徒
			GameSleep(50);
		}
		for(i=0;i<4;i++)
		{
			blt((368-(i*16)),336,32,32,96,96,32,96,pdc);//司徒
			GameSleep(50);
			blt((352-(i*16)),336,32,32,64,96,0,96,pdc);//司徒
			GameSleep(50);
		}
		blt(256,320,32,32,64,128,0,128,pdc);//司徒
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,5,0,62);
		st_dialog=58;
	}
	else if(st_dialog==58)
	{
		Dialog(4,21,41,0,63);
		st_dialog=59;
	}
	else if(st_dialog==59)
	{
		Dialog(4,0,1,0,64);
		st_dialog=60;
	}
	else if(st_dialog==60)
	{
		Dialog(4,0,41,0,65);
		st_dialog=61;
	}
	else if(st_dialog==61)
	{
		Dialog(4,0,5,0,66);
		st_dialog=62;
	}
	else if(st_dialog==62)
	{
		Dialog(4,0,31,0,67);
		st_dialog=63;
	}
	else if(st_dialog==63)
	{
		Dialog(4,0,5,0,68);
		st_dialog=64;
	}
	else if(st_dialog==64)
	{
		Dialog(4,0,31,0,69);
		st_dialog=65;
	}
	else if(st_dialog==65)
	{
		m_info_prop2=0;
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,2,0,70);
		st_dialog=66;
	}
	else if(st_dialog==66)
	{
		Dialog(4,0,31,0,71);
		st_dialog=67;
	}
	else if(st_dialog==67)
	{
		Dialog(4,0,5,0,72);
		st_dialog=68;
	}
	else if(st_dialog==68)
	{
		Dialog(4,0,41,0,90);
		st_dialog=69;
	}
	else if(st_dialog==69)
	{
		Dialog(4,0,5,0,73);
		st_dialog=70;
	}
	else if(st_dialog==70)
	{
		Dialog(4,21,0,0,74);
		st_dialog=71;
	}
	else if(st_dialog==71)
	{
		Dialog(4,0,1,0,75);
		st_dialog=72;
	}
	else if(st_dialog==72)
	{
		Dialog(4,0,1,0,76);
		st_dialog=73;
	}
	else if(st_dialog==73)
	{
		Dialog(4,21,31,0,77);
		st_dialog=74;
	}
	else if(st_dialog==74)
	{
		Dialog(4,0,2,0,78);
		st_dialog=75;
	}
	else if(st_dialog==75)
	{
		Dialog(4,21,0,0,79);
		st_dialog=76;
	}
	else if(st_dialog==76)
	{
		Dialog(4,0,31,0,80);
		st_dialog=77;
	}
	else if(st_dialog==77)
	{
		Dialog(4,0,41,0,81);
		st_dialog=78;
	}
	else if(st_dialog==78)
	{
		//显示叹号
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(256,288,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		resetblt();
		blt(256,288,35,33,0,0,140,0,pdc);//叹
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,3,0,82);
		st_dialog=79;
	}
	else if(st_dialog==79)
	{//司徒溜走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//司徒
		blt(256,320,32,32,64,160,0,160,pdc);//司徒
		for(i=0;i<4;i++)
		{
			oldlx=256;
			oldly=288;
			oldx=35;
			oldy=33;
			blt((256+(i*16)),320,32,32,96,160,32,160,pdc);//司徒
			GameSleep(50);
			blt((272+(i*16)),320,32,32,64,160,0,160,pdc);//司徒
			GameSleep(50);
		}
		for(i=0;i<10;i++)
		{
			blt(368,(320+(i*16)),32,32,96,64,32,64,pdc);//司徒
			GameSleep(50);
			blt(368,(336+(i*16)),32,32,64,64,0,64,pdc);//司徒
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,22,0,0,83);
		st_dialog=80;
	}
	else if(st_dialog==80)
	{
		lclose();
		current.x=103;
		current.y=155;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		MemDC.SelectObject(hbmp_4);//白莲教团
		resetblt();
		CDC *pdc=GetDC();
		blt(272,112,96,64,192,0,0,0,&BuffDC);
		lopen();
		for(i=0;i<8;i++)
		{
			blt(272,(112+(i*16)),96,64,192,0,0,0,pdc);
			GameSleep(100);
			blt(272,(120+(i*16)),96,64,288,0,96,0,pdc);
			GameSleep(100);
		}
		GameSleep(500);
		lclose();
		current.x=114;
		current.y=170;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);//将地图复制到缓冲页面	
		//准备人物。（荻娜、卡诺、露丝）
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		blt(224,256,32,48,128,144,32,144,&BuffDC);//荻娜
		MemDC.SelectObject(hbmp_5);//卡诺
		resetblt();
		blt(320,256,32,48,96,48,0,48,&BuffDC);//卡诺
		MemDC.SelectObject(hbmp_1);//露丝
		resetblt();
		blt(256,208,32,48,96,0,0,0,&BuffDC);//露丝
		lopen();
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,41,0,84);
		st_dialog=81;
	}
	else if(st_dialog==81)
	{//露丝走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//露丝
		resetblt();
		blt(256,144,32,48,96,144,0,144,pdc);//露丝
		for(i=0;i<6;i++)
		{
			blt((256+(i*16)),208,32,48,128,144,32,144,pdc);//露丝
			GameSleep(50);
			blt((272+(i*16)),208,32,48,160,144,64,144,pdc);//露丝
			GameSleep(50);
		}
		for(i=0;i<16;i++)
		{
			blt(352,(208-(i*16)),32,48,128,96,32,96,pdc);//露丝
			GameSleep(50);
			blt(352,(192-(i*16)),32,48,160,96,64,96,pdc);//露丝
			GameSleep(50);
		}
		GameSleep(200);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,93);
		st_dialog=82;
	}
	else if(st_dialog==82)
	{
		st_dialog=83;
		Dialog(5,0,0,0,85);//参数5表示存盘
	}
	else if(st_dialog==83)
	{
		st=5;//第四段
		st_sub1=0;
		st_dialog=0;
		vclose();
		Dialog(2,0,0,0,93);
		Para4Init();
	}
}//这里是本阶段程序的结束部分
////////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para3Accident(int type)
{
m_info_st=9;//对话态
//m_oldinfo_st=9;
if(st_sub1==0){
	//以下部分是大港的人物
////////////////////////////////////////////////////////////////////////////////
	if(type==174) {//去诊所
		Dialog(4,0,31,0,4);
		st_dialog=30;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(type==175){//向村民打听道路
		Dialog(4,0,0,0,2);
		st_dialog=20;
	}
	else if(type==176){//镇长办公室门口的卫兵
		Dialog(4,0,0,0,89);
		st_dialog=20;
	}
	else if(type==178){//西蒙镇长
		Dialog(4,0,11,0,3);
		st_dialog=20;
	}//大港镇的人物结束
	//以下部分封闭地界
	else if(type==205){//地界E
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==206){//地界F
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==211){//地界K
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==215){//地界O
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==222){//地界V
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	//以下是对108地图的处理
	else if(type==188){//未去山洞时阻拦上山的地界
		Dialog(4,0,31,0,6);
		st_dialog=20;
	}
	else if(type==236){//山洞
		Dialog(4,0,31,0,7);//哇，怎么有这么大的风……
		st_dialog=31;
	}//这里是一个关键的情节进展点
	//以下部分处理地图切换
	else if(type==224){//地界X（地图108下方的道路）对应地界Y
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		base_x=194;//
		base_y=287;//设定地图基准点
		current.y=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==225){//地界Y（地图104上方的道路）对应地界X
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108a.map","map\\map108a.npc");//调入地图
		base_x=190;//
		base_y=187;//设定地图基准点
		current.y=185;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==207){//地界G（地图103右方的道路）对应地界J
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.x=15;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==210){//地界J（地图104左方的道路）对应地界G
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map103c.map","map\\map103d.npc");//调入地图
		base_x=94;//设定地图基准点
		base_y=287;///////////////
		current.x=180;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
}//st_sub1==0的部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){//响应第二段的地图事件
	//下面一段用于封闭地界
	if(type==222){//地界V
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==231){//地界金
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==230){//地界丁
		Dialog(4,0,31,0,94);
		st_dialog=20;
	}
	else if(type==224){//地界X
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	//事件响应
	else if(type==226){//山洞
		Dialog(2,0,0,0,11);
		st_dialog=20;
	}
	else if(type==186){//不说话的卫兵
		Dialog(4,0,0,0,89);
		st_dialog=20;
	}
	else if(type==223){//火山的景色
		StopActTimer();
		lclose();
		MemDC.SelectObject(hbmp_6);//火山的图片
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		lopen();
		Dialog(2,0,0,0,25);
		st_dialog=20;
	}
	else if(type==181){//村民1
		Dialog(4,0,0,0,13);
		st_dialog=20;
	}
	else if(type==182){//村民2
		Dialog(4,0,0,0,13);
		st_dialog=20;
	}
	else if(type==183){//村民3
		Dialog(4,0,0,0,13);
		st_dialog=20;
	}
	else if(type==184){//村民4
		Dialog(4,0,0,0,13);
		st_dialog=20;
	}
	else if(type==185){//村民5
		Dialog(4,0,0,0,13);
		st_dialog=20;
	}
	else if(type==187){//卡洛德，情节进展的关键
		Dialog(4,0,31,0,12);
		st_dialog=30;
	}
}////st_sub1==1的部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==2){//响应第三段的地图事件
	//下面一段用于封闭地界
	if(type==222){//地界V
		Dialog(4,0,31,0,24);
		st_dialog=20;
	}
	else if(type==231){//地界金
		Dialog(4,0,31,0,24);
		st_dialog=20;
	}
	else if(type==230){//地界丁
		Dialog(4,0,31,0,94);
		st_dialog=20;
	}
	else if(type==191){//下方禁止出村的地界
		Dialog(4,0,31,0,24);
		st_dialog=20;
	}
	//事件响应
	else if(type==226){//山洞
		Dialog(2,0,0,0,11);
		st_dialog=20;
	}
	else if(type==186){//不说话的卫兵
		Dialog(4,0,0,0,89);
		st_dialog=20;
	}
	else if(type==223){//火山的景色
		StopActTimer();
		lclose();
		MemDC.SelectObject(hbmp_6);//火山的图片
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		lopen();
		Dialog(2,0,0,0,25);
		st_dialog=20;
	}
	else if(type==181){//村民1
		Dialog(4,0,0,0,18);
		if((st_progress%2)!=0){
			st_progress*=2;
		}//不是2的整数倍，则乘以2。
		st_dialog=20;
	}
	else if(type==182){//村民2
		Dialog(4,0,0,0,19);
		if((st_progress%3)!=0){
			st_progress*=3;
		}//不是3的整数倍，则乘以3
		st_dialog=20;
	}
	else if(type==183){//村民3
		Dialog(4,0,0,0,20);
		if((st_progress%5)!=0){
			st_progress*=5;
		}//不是5的整数倍，则乘以5
		st_dialog=20;
	}
	else if(type==184){//村民4
		Dialog(4,0,0,0,21);
		if((st_progress%7)!=0){
			st_progress*=7;
		}//不是7的整数倍，则乘以7
		st_dialog=20;
	}
	else if(type==185){//村民5
		Dialog(4,0,0,0,22);
		if((st_progress%11)!=0){
			st_progress*=11;
		}//不是11的整数倍，则乘以11
		st_dialog=20;
	}
	else if(type==187){//卡洛德，情节进展的关键
		if(st_progress==2310){//说明所有村民都问话了，可以继续。
			Dialog(4,16,0,0,26);
			st_dialog=30;
		}
		else{//有人漏掉了
			Dialog(4,16,0,0,17);
			st_dialog=20;
		}
	}
}////st_sub1==2的部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==3){//响应第四段的地图事件
	//下面一段用于封闭地界
	if(type==222){//地界V
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==231){//地界金
		Dialog(4,0,31,0,86);
		st_dialog=20;
	}
	else if(type==230){//地界丁
		Dialog(4,0,31,0,94);
		st_dialog=20;
	}
	else if(type==189){//遇见荻娜时的地界
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(400,176,35,33,0,0,105,0,pdc);//叹
		GameSleep(1000);
		blt(400,176,35,33,0,0,140,0,pdc);//叹
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,30);
		st_dialog=30;
	}
	//事件响应
	else if(type==226){//山洞
		Dialog(2,0,0,0,11);
		st_dialog=20;
	}
	else if(type==186){//不说话的卫兵
		Dialog(4,0,0,0,89);
		st_dialog=20;
	}
	else if(type==223){//火山的景色
		StopActTimer();
		lclose();
		MemDC.SelectObject(hbmp_6);//火山的图片
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		lopen();
		Dialog(2,0,0,0,25);
		st_dialog=20;
	}
	else if(type==181){//村民1
		Dialog(4,0,0,0,18);
		st_dialog=20;
	}
	else if(type==182){//村民2
		Dialog(4,0,0,0,19);
		st_dialog=20;
	}
	else if(type==183){//村民3
		Dialog(4,0,0,0,20);
		st_dialog=20;
	}
	else if(type==184){//村民4
		Dialog(4,0,0,0,21);
		st_dialog=20;
	}
	else if(type==185){//村民5
		Dialog(4,0,0,0,22);
		st_dialog=20;
	}
	else if(type==187){//卡洛德
		Dialog(4,16,0,0,28);
		st_dialog=20;
	}
}////st_sub1==3的部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==4){//响应第五段的地图事件
	//封闭地界
	if(type==225){//地界Y
		Dialog(4,0,31,0,87);
		st_dialog=20;
	}
	if(type==215){//地界O
		Dialog(4,0,31,0,87);
		st_dialog=20;
	}
	if(type==211){//地界K
		Dialog(4,0,31,0,87);
		st_dialog=20;
	}
	if(type==207){//地界G
		Dialog(4,0,31,0,87);
		st_dialog=20;
	}
	//事件响应
	else if(type==226){//山洞
		Dialog(2,0,0,0,11);
		st_dialog=20;
	}
	else if(type==191){//卡诺：“送荻娜回诊所要紧。”
		Dialog(4,0,31,0,87);
		st_dialog=20;
	}
	else if(type==174){//诊所
		Dialog(4,36,0,0,34);
		st_dialog=30;
	}//此处是情节发展的转折点。
	//以下用于地图切换
	else if(type==224){//地界X（地图108下方的道路）对应地界Y
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		base_x=194;//设定小地图上的基准点偏移量
		base_y=287;///////////////
		current.y=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==210){//地界J（地图104左方的道路）对应地界G
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map103c.map","map\\map103e.npc");//调入地图
		base_x=94;//设定小地图上的基准点偏移量
		base_y=287;///////////////
		current.x=180;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
}////st_sub1==3的部分结束
////////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para3Timer()
{
	processenemies();
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████